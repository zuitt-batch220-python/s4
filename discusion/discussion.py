class Car:
    def __init__(self, brand, model, year_of_make):
        self.brand = brand
        self.model = model
        self.year_of_make = year_of_make

        self.fuel = "Gasoline"
        self.fuel_level = 0

    # methods

    def fill_fuel(self):
        print(f"Current fuel level: {self.fuel_level}")
        print("Filling up the fuel tank...")
        self.fuel_level = 100
        print(f"New fuel level: {self.fuel_level}")


"""new_car = Car("Honda", "Jazz", 2020)
print(f"My car is a {new_car.brand} {new_car.model} {new_car.year_of_make}")
"""


# [Section] Encapsulation
# a form of data hiding in Python
class Person():
    def __init__(self):
        # "_" denotes that the attribute should only be modified manually inside the declared class
        self._name = "John Doe"
        self._age = 28

    # setter methods are a way to set an instance of a class attribute
    def set_name(self, name):
        self._name = name

    # getter methods are a way to retrieve a class
    def get_name(self):
        print(f"Name of Person: {self._name}")

    def set_age(self, age):
        self._age = age

    def get_age(self):
        print(f"Age of Person: {self._age}")


p1 = Person()
p1.get_name()
p1.set_name("Jane Doe")
p1.get_name()
print(p1._name)

# Activity
p1.get_age()
p1.set_age(30)
p1.get_age()


# [Section] Inheritance
class Employee(Person):
    def __init__(self, employee_id, date_hired):
        super().__init__()
        self._employee_id = employee_id
        self._date_hired = date_hired

    def get_employee_id(self):
        print(f"The employee_id is {self._employee_id}")

    def set_employee_id(self, employee_id):
        self._employee_id = employee_id

    def get_details(self):
        print(f"{self._employee_id} belongs to {self._name}")

    def get_date_hired(self):
        print(f"The employee was hired last {self._date_hired}")


emp1 = Employee("Emp-001", 2012)
emp1.get_employee_id()
emp1.get_details()
emp1.get_date_hired()


# Create a new class called Student that inherits the Person with additional attributes and methods.
# Attribute: Student No. Course, Year Level
# Methods: get_details = " __ is currently in __ year taking up ___. With getters and setters

class Student(Person):
    def __init__(self, student_no, course, year_level):
        super().__init__()
        self._student_no = student_no
        self._course = course
        self._year_level = year_level

    def get_student_no(self):
        print(f"{self._name} student number is {self._student_no}")

    def set_student_no(self, student_no):
        self._student_no = student_no

    def get_course(self):
        print(f"{self._name} course is {self._course}")

    def set_course(self, course):
        self._course = course

    def get_year_level(self):
        print(f"{self._name} year level is {self._year_level}")

    def set_year_level(self, year_level):
        self._year_level = year_level

    def get_details(self):
        print(f"{self._name} is currently in {self._year_level} year taking up {self._course}")


std1 = Student(1001, "Financial Management", "4th")
std1.get_details()


# [Section] Polymorphusm
# since not all methdos are applicable to the child element when inherited, some need to be re-defined/re-implemented

# Using Function
class Admin():
    def is_admin(self):
        print(True)

    def user_type(self):
        print("Admin User")


class Customer():
    def is_admin(self):
        print(False)

    def user_type(self):
        print("Regular User")


# define a function that will check the taken object as argument
def test_function(obj):
    obj.is_admin()
    obj.user_type()


user_admin = Admin()
user_customer = Customer()

test_function(user_admin)
test_function(user_customer)


# what happens is that the test_function would call methods of the objects passed to it hence allowing it to have different outputs depending on the objects

# Using Classes
class Team_Lead():
    def occupation(self):
        print("Team Lead")

    def hasAuth(self):
        print(True)


class Team_Member():
    def occupation(self):
        print("Team Member")

    def hasAuth(self):
        print(False)


tl1 = Team_Lead()
tm1 = Team_Member()

# using for loop to iterate in a sq of classes
for person in (tl1, tm1):
    # will access the ccupation method for each of the iterated class
    person.occupation()


# Using Inheritance
class Zuitt():
    def tracks(self):
        print("We are currently offering 3 tracks(developer career, pi-shaped, and short courses")

    def num_of_hours(self):
        print("Learn web-dev in 360 hours")


class Developer(Zuitt):
    def num_of_hours(self):
        print("Learn the basic of web-dev in 240 hours")


class Pi_Shaped(Zuitt):
    def num_of_hours(self):
        print("Learn the basic of web-dev in 140 hours")


class Short_Courses(Zuitt):
    def num_of_hours(self):
        print("Learn the basic of web-dev in 20 hours")


course1 = Developer()
course2 = Pi_Shaped()
course3 = Short_Courses()

for course in (course1, course2, course3):
    course.num_of_hours()

# [Section] Abstraction
from abc import ABC, abstractclassmethod


class Polygon(ABC):
    @abstractclassmethod
    def print_number_of_sides(self):
        pass
    # pass denotes that the method does not do anything


class Triangle(Polygon):
    def __init__(self):
        super().__init__()

    def print_number_of_sides(self):
        print(f"This is polygon has 3 sides")


class Pentagon(Polygon):
    def __init__(self):
        super().__init__()

    def print_number_of_sides(self):
        print(f"This is polygon has 5 sides")


shape1 = Triangle()
shape2 = Pentagon()
shape1.print_number_of_sides()
shape2.print_number_of_sides()
