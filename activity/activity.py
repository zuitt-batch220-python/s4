# Activity

"""
1. Create an abstract class called Animal that has the following abstract methods
Abstract Methods:
* eat(food)
* make_sound()
"""

# Used abstractmethod because abstractclassmethod is deprecated as of Python3
from abc import ABC, abstractmethod


class Animal(ABC):

    @abstractmethod
    def eat(self, food):
        pass

    @abstractmethod
    def make_sound(self):
        pass


"""
2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
Properties:
* name
* breed
* age

Methods:
* getters and setters
* implementation of abstract methods
* call()
"""


# Inheriting to Dog Class
class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):
        print("Bark! Woof! Arf!")

    def call(self):
        print(f"Here {self.get_name()}!")

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_breed(self):
        return self._breed

    def set_breed(self, breed):
        self._breed = breed

    def get_age(self):
        return self._age

    def set_age(self, age):
        self._age = age


# Inheriting to Cat Class
class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age

    def eat(self, food):
        print(f"Serve me {food}")

    def make_sound(self):
        print("Miaow! Nyaw! Nyaaaaa!")

    def call(self):
        print(f"{self.get_name()}, come on!")

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_breed(self):
        return self._breed

    def set_breed(self, breed):
        self._breed = breed

    def get_age(self):
        return self._age

    def set_age(self, age):
        self._age = age


# Test
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()
